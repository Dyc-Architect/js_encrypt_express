FROM node
LABEL maintainer="15596870128@163.com"

WORKDIR /data
COPY . .
EXPOSE 3000

ENTRYPOINT ["node","bin/www"]
